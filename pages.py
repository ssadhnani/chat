import base
from oauth2client.client import OAuth2WebServerFlow
import UserList
import datetime
import MsgStore

flow = OAuth2WebServerFlow(client_id='973157361069-nttdgrei5cghsq7g9ajufejh43tlr3tm.apps.googleusercontent.com',
                           client_secret='1PHb4Rne0Nt38UgcYh3M70kz',
                           scope='https://www.googleapis.com/auth/userinfo.email',
                           redirect_uri='http://localhost:8080/return')

current_user = None
chosen_user = None

class LoginPage(base.Base):
	
	def get(self):
		self.render('login.html')

class Login(base.Base):
	
	def get(self):
		auth_url = flow.step1_get_authorize_url()
		self.redirect(auth_url)

class Return(base.Base):
	
	def get(self):
		code = self.request.get('code',None)
		if code :
			credentials = flow.step2_exchange(self.request.get("code"))
			email = credentials.id_token['email']
			email = str(email)
			global current_user 
			current_user = email
			email_set = {email}
			prev_user = UserList.UserList().all().filter("user = ",email)
			user_set = set()
			for items in prev_user:
				user_set.add(items.user)
			if email_set - user_set == email_set :
				user_list_obj = UserList.UserList( user = email )
				user_list_key = user_list_obj.put()
			self.redirect('/chatlist')
		else :
			self.redirect('/')

class ChatList(base.Base):
	def get(self):
		global chosen_user
		chosen_user = self.request.get('chosen_user',None)
		if chosen_user :
			self.redirect('/chatbox')
		else:
			global current_user
			current_user_temp_val = {'user':current_user}
			user_list_obj = UserList.UserList().all().filter("user != ",current_user)
			template_values = {'data':user_list_obj}
			template_values.update(current_user_temp_val)
			self.render('chatlist.html',template_values)

class ChatBox(base.Base):
	
	def get(self):
		global current_user
		global current_user
		msg = self.request.get("msg")
		msg_time = datetime.datetime.now()
		sender = current_user
		receiver = chosen_user
		msg_store_obj = MsgStore.MsgStore( msg = msg, msg_time = msg_time, sender = sender, receiver = receiver)
		msg_store_key = msg_store_obj.put()
		recent = {'recent_msg': msg}
		send_msg_obj = MsgStore.MsgStore().all().order('msg_time').filter("sender = ", current_user).filter("receiver = ", chosen_user)
		received_msg_obj = MsgStore.MsgStore().all().order('msg_time').filter("sender = ", chosen_user).filter("receiver = ", current_user)
		# msg_obj = send_msg_obj + received_msg_obj
		key = {'data': send_msg_obj}
		key2 = {'dada': received_msg_obj}
		key.update(recent)
		key.update(key2)
		self.render('chatbox.html',key)

