import webapp2
import urls

application = webapp2.WSGIApplication( urls.urls, debug = True )

def main():
	#current_user = None
	run_wsgi_app(application)

if __name__ == "__main__":
	main()